module Language where

-- expressões
data Exp =  Constant Int
        |  Variable String
        |  Minus Exp Exp
        |  Plus Exp Exp
        |  Div Exp Exp
        |  Times Exp Exp
        |  Compare Exp Exp
        |  Greater Exp Exp
        |  Less Exp Exp
        deriving (Show)

-- comandos
data Com =  Assign String Exp
        |  Seq [Com]
        |  Cond Exp Com Com
        |  While Exp Com
        |  Declare String Exp Com
        |  Print Exp
        |  EmptyCom
        deriving (Show)