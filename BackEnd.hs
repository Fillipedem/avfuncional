module BackEnd where

import Language

--
cond :: Int -> Bool
cond 0 = False
cond _ = True

-- estruturas e funções para manipular o ambiente
type Location = Int
type Stack = [(String, Int)]

fetch :: String -> Stack -> Int
fetch name [] = error ("Variavel: " ++ name ++ ", não está declarada!")
fetch name ((n, v):vs) = if n == name
                 then v
                 else fetch name vs

put :: String -> Int -> Stack -> Stack
put name val [] = [(name, val)]
put name val ((n, v):vs) = if n == name
                    then (name, val):vs
                    else (n, v) : put name val vs

-- monad State Output
newtype StateOutput a = StOut (Stack -> (a, Stack, [String]))

unStOut (StOut f) = f


-- Sem a definição de Functor e Applicative ocorre um erro
instance Functor StateOutput where
    fmap f (StOut st) = StOut $ \s ->
            let (a, s', str) = st s
            in (f a, s', str)

instance Applicative StateOutput where
    pure = return

    StOut st <*> StOut st' = StOut $ \s ->
        let (f, s1, str1) = st s
            (a , s2, str2) = st' s
        in ((f a), s2, str2)

-- Definição da instancia monad
instance Monad StateOutput where
    return x = StOut $ \s -> (x, s, [])
    
    StOut st >>= f = StOut $ \s ->
        let (a1, s1, str1) = st s
            (a2, s2, str2) = unStOut (f a1) s1  
        in (a2, s2, str1 ++ str2)

-- Funções para modificar o estado do Stack no monada
get :: String -> StateOutput Int
get name = StOut $ \st -> 
    let val = fetch name st
    in (val, st, [])

write :: String -> Int -> StateOutput Int
write name val = StOut $ \st -> 
    let st' = put name val st
    in (val, st', [])

push :: String -> Int -> StateOutput ()
push n v = StOut $ \st -> ((), (n, v):st, [])

pop :: StateOutput ()
pop = StOut $ \st -> ((), tail st, [])

output :: Show a => a -> StateOutput ()
output v = StOut (\n -> ((), n, [show v]))


-- Evall - Expressões
evall :: Exp -> StateOutput Int
evall exp = case exp of
    Constant n -> return n
    Variable x -> get x
    Minus exp1 exp2 -> do e1 <- evall exp1
                          e2 <- evall exp2
                          return (e1 - e2)
    Plus exp1 exp2 -> do e1 <- evall exp1
                         e2 <- evall exp2
                         return (e1 + e2)
    Div exp1 exp2 -> do e1 <- evall exp1
                        e2 <- evall exp2
                        return (quot e1 e2)
    Times exp1 exp2 -> do e1 <- evall exp1
                          e2 <- evall exp2
                          return (e1*e2)
    Compare exp1 exp2 -> do e1 <- evall exp1
                            e2 <- evall exp2
                            if e1 == e2
                            then return 1
                            else return 0
    Greater exp1 exp2 -> do e1 <- evall exp1
                            e2 <- evall exp2
                            if e1 > e2
                            then return 1
                            else return 0
    Less exp1 exp2 -> do e1 <- evall exp1
                         e2 <- evall exp2
                         if e1 < e2
                         then return 1
                         else return 0


-- Exec - Comandos
exec :: Com -> StateOutput ()
exec stmt = case stmt of
    Assign name e -> do val <- evall e
                        write name val
                        return ()
    Seq [] -> return ()
    Seq (x:xs) -> do exec x
                     exec (Seq xs)
                     return ()
    Cond e ifcom EmptyCom -> do val <- evall e
                                if cond val
                                then exec ifcom
                                else return ()
    Cond e ifcom elcom -> do val <- evall e
                             if cond val
                             then exec ifcom
                             else exec elcom
    While exp com -> do val <- evall exp
                        if cond val
                        then do exec com 
                                exec (While exp com)
                                return ()
                        else return ()
    Print exp -> do val <- evall exp
                    output val
    Declare nm e stmt -> do val <- evall e
                            push nm val
                            exec stmt
                            pop

-- Função para executar o programa
execProgram inp = output
            where (a, state, output) = unStOut (exec inp) []