import BackEnd
import FrontEnd
import System.Environment (getArgs)


parseLanguage :: String -> IO String
parseLanguage [] = return []
parseLanguage inp = case parseProgram inp of
                        Left err -> return (show err)
                        Right cod -> return (show (execProgram cod))

-- Interação
interactWith function inputFile = do 
    input <- readFile inputFile
    output <- function input
    putStrLn output

main = mainWith myFunction where 
        mainWith function = do
                args <- getArgs
                case args of
                    [input] -> interactWith function input
                    _ -> putStrLn "Argumentos incorretos"
        myFunction = parseLanguage


