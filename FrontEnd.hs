-- Parser
module FrontEnd where

import Language
import System.IO
import Control.Monad
import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Expr
import Text.ParserCombinators.Parsec.Language
import Text.ParserCombinators.Parsec.Combinator
import qualified Text.ParserCombinators.Parsec.Token as Token


-- Definindo tokens
languageDef =
  emptyDef { Token.commentStart    = "/*"
           , Token.commentEnd      = "*/"
           , Token.commentLine     = "//"
           , Token.identStart      = letter
           , Token.identLetter     = alphaNum
           , Token.reservedNames   = [ "if"
                                     , "then"
                                     , "else"
                                     , "while"
                                     , "do"
                                     , "in"
                                     , "declare"
                                     , "print"
                                     ]
           , Token.reservedOpNames = ["+", "-", "*", "/", ":="
                                     , "<", ">", "="
                                     ]
           }

-- lexer
lexer = Token.makeTokenParser languageDef

identifier = Token.identifier lexer
reserved   = Token.reserved   lexer
reservedOp = Token.reservedOp lexer
parens     = Token.parens     lexer
braces     = Token.braces     lexer
semiSep1    = Token.semiSep1    lexer 
integer    = do x <- Token.integer lexer -- retorna um Integer, reaizamos a conversão 
                return (fromInteger x)   -- porque o tipo é Constant Int
semi       = Token.semi       lexer
whiteSpace = Token.whiteSpace lexer



-- Expressões
expression :: Parser Exp
expression = buildExpressionParser expOperators expTerm

expOperators = [[Infix  (reservedOp "*"   >> return (Times)) AssocLeft,
               Infix  (reservedOp "/"   >> return (Div)) AssocLeft]
              , [Infix  (reservedOp "+"   >> return (Plus)) AssocLeft,
                 Infix  (reservedOp "-"   >> return (Minus)) AssocLeft]
              , [Infix  (reservedOp ">"   >> return (Greater)) AssocLeft,
                 Infix  (reservedOp "<"   >> return (Less)) AssocLeft,
                 Infix  (reservedOp "="   >> return (Compare)) AssocLeft]]

expTerm =  parens expression
            <|> liftM Variable identifier
            <|> liftM Constant integer


statement :: Parser Com
statement = assignStmt
        <|> seqStmt
        <|> ifElseStmt
        <|> whileStmt
        <|> declareStmt
        <|> printStmt

assignStmt :: Parser Com
assignStmt = do id <- identifier
                reservedOp ":="
                exp <- expression
                return (Assign id exp) 

seqStmt :: Parser Com
seqStmt = braces seqStmt'

seqStmt' :: Parser Com
seqStmt' = do list <- (sepBy1 statement semi)
              return $ if length list == 1 
                       then head list 
                       else Seq list

ifElseStmt :: Parser Com
ifElseStmt = do reserved "if"
                exp <- expression
                reserved "then"
                ifCom <- statement
                reserved "else"
                elseCom <- statement
                return (Cond exp ifCom elseCom)

whileStmt :: Parser Com
whileStmt = do reserved "while"
               exp <- expression
               com <- statement
               return (While exp com)

declareStmt :: Parser Com
declareStmt = do reserved "declare"
                 x <- identifier
                 reservedOp "="
                 exp <- expression
                 reserved "in"
                 com <- statement
                 return (Declare x exp com)

printStmt :: Parser Com
printStmt = do reserved "print"
               exp <- expression
               return (Print exp)


-- Função para parse
parseProgram = parse statement "Erro!"